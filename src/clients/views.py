import os
import uuid

from django.conf import settings
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.db.models import Q
from django.http import HttpResponseRedirect, Http404
from django.shortcuts import render, redirect, get_object_or_404


from .models import Client
from .forms import ClientForm
import tasks as task

# Create your views here.


@login_required
def client_create(request):
    createForm = ClientForm(request.POST or None, request.FILES or None)

    if createForm.is_valid():
        instance = createForm.save(commit=False)
        instance.save()

        messages.success(request, "Succesfully created")
        return HttpResponseRedirect(instance.get_absolute_url())

    context = {
        "form": createForm,
    }

    return render(request, "clients/edit.html", context)


def client_list(request, _reportpath=''):
    # filter clients
    fltr_firstname = request.GET.get("qfirstname") or ''
    fltr_secondname = request.GET.get("qsecondname") or ''

    query_list = Client.objects.filter(
                        Q(first_name__icontains=fltr_firstname) &
                        Q(second_name__icontains=fltr_secondname)
                        )

    orderField = request.GET.get("o")

    if orderField:
        query_list = query_list.order_by(orderField)

    template = "clients/list.html"
    context = {
        "record_list": query_list,
        "title": "Clients List",
        "report_path": _reportpath,
    }

    return render(request, template, context)


def client_detail(request, id=None):
    instance = get_object_or_404(Client, id=id)

    context = {
        "title":  instance,
        "instance": instance,
    }

    return render(request, "clients/detail.html", context)


@login_required
def client_update(request, id=None):
    instance = get_object_or_404(Client, id=id)
    updateForm = ClientForm(request.POST or None, request.FILES or None, instance=instance)

    if updateForm.is_valid():
        instance = updateForm.save(commit=False)
        instance.save()
        messages.success(request, "Successfuly saved")

        return HttpResponseRedirect(instance.get_absolute_url())

    context = {
        "title":  instance,
        "instance": instance,
        "form": updateForm,
    }

    return render(request, "clients/edit.html", context)


@login_required
def client_delete(request, id=None):
    instance = get_object_or_404(Client, id=id)
    instance.delete()
    messages.success(request, "Successfuly deleted")

    return redirect("clients:list")


def download_report(request):
    filecode = str(uuid.uuid4())[:6].replace('-', '').lower()
    filename = "report_{}.xlsx".format(filecode)
    reportfile = os.path.join(settings.MEDIA_ROOT, "reports", filename)

    # print("DEBUG: {}\n".format(reportfile))

    task.generate_report.delay(reportfile)

    return client_list(request, settings.MEDIA_URL + "reports/{}".format(filename))


def vote_for_client(request, id=None):
    if not id:
        return Http404

    task.like_the_client.delay(id)

    return redirect('clients:detail', id=id)


def vote_for_client_poll(request, id=None):
    if not id:
        return Http404

    task.like_the_client.delay(id)

    return redirect('clients:poll')


def client_poll(request):
    query_list = Client.objects.all()

    template = "clients/poll.html"
    context = {
        "record_list": query_list,
        "title": "Clients Poll",
    }

    return render(request, template, context)
